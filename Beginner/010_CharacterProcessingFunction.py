''' 문자열 처리 함수 '''

python = 'Python is Amazing'
print(python.lower()) # 소문자 변환
print(python.upper()) # 대문자 변환
print(python[0].isupper()) # index 0 번 대문자 확인 True
print(len(python)) #문자 갯수

print(python.replace('Python', 'PHP')) # 특정 문자열 변환

# 없으면 컴파일 에러출력
index = python.index('n') # n 위치 확인
print(index)
index = python.index('n', index + 1) # n 두번째 위치 확인
print(index)

# 없으면 -1 출력
print(python.find('JAVA')) # 없으면 -1 반환
print(python.find('n')) # 첫 번째 n 인덱스 출력

# 없으면 0 출력
print(python.count('n')) # 2
