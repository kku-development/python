''' 변수 '''

# 애완동물을 소개해 주세요.
animal: str     = '강아지'
name: str       = '미소'
age: int        = 10
hobby: str      = '산책'
isAdult: bool   = age >= 10

print('우리집 '+ animal +'의 이름은 '+ name +'이예요.')
print('미소는 '+ str(age) +'살이며, '+ hobby +'을 아주 좋아합니다.')
# 콤마(,) 사용시 str 사용하지 않아도 되지만 출력시 한 칸씩 띄어 진다.
print('미소는 ', age, '살이며, ', hobby, '을 아주 좋아합니다.')
print('미소는 어른일까요? '+ str(isAdult))