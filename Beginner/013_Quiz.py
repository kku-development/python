''' 
Quiz) 사이트별로 비밀번호를 만들어 주는 프로그램을 작성하시오

예) http://naver.com
규칙1 : http:// 부분은 제외 => naver.com
규칙2 : 처음 만나는 점(.) 이후 부분은 제외 naver
규칙3 : 남은 글자 중 처음 세자리 + 굴자 갯수 + 글자 내 'e' 갯수 + '!' 구성

예) 새성된 비밀번호 : nav51!

'''

url: str = 'http://google.com'
answer: str 

# 규칙 1
answer = url[7:]
# answer = url.replace('http://', '')
print(answer)

# 규칙 2
dotIndex: int = answer.index('.')
answer = answer[:dotIndex]
print(answer)

# 규칙 3
resultA: str = answer[:3]
resultB: int = len(answer)
resultC: int = answer.count('e')
resultD: str = '!'
password = resultA + str(resultB) + str(resultC) + resultD

print('{0} 의 비밀번호 {1} 입니다.'.format(url, password))
print(f'{url} 의 비밀번호 {password} 입니다.')
