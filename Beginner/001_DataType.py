''' 자료형 '''

# 숫자형
print('========== 숫자형(Interger)')
print(5)
print(-10)
print(3.14)
print(1000)
print(5 + 3)
print(2 * 8)
print(3 * (3 + 1))

# 문자형
print('========== 문자형(String)')
print('풍선')
print("나비")
print('반복 * 10, '*10) # 지정 반복

# 참 / 거짓
print('========== 참거짓(Boolean))')
print(5 > 10)
print(5 < 10)
print(True) # 첫 글자 대문자 사용
print(False) 
print(not True) # 부정
print(not False)
print(not (5 > 10))
